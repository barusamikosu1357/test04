
function regist() {
	const p1 = document.getElementById("p1");
	const title = document.getElementById("title").value;
	const dailyReport = document.getElementById("dailyReport").value;
	if (title.length == 0 || dailyReport.length == 0) {
		p1.style.display = "block";
	} else {
		var form = document.getElementById("register");
		p1.style.display = "none";
		form.action = "/regist"
		form.submit();
	}

}

window.onload = function() {
	var today = new Date();
	today.setDate(today.getDate());
	var yyyy = today.getFullYear();
	var mm = ("0" + (today.getMonth() + 1)).slice(-2);
	var dd = ("0" + today.getDate()).slice(-2);
	document.getElementById("today").value = yyyy + '-' + mm + '-' + dd;
}