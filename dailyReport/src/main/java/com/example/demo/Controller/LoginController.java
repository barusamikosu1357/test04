package com.example.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
	@Autowired
	private JdbcTemplate template;

	@RequestMapping("/Login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("Login");
		return mav;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView clickLogin(ModelAndView mav, @ModelAttribute(name = "login_id") String login_id,
			@ModelAttribute(name = "password") String password) {
		
		// ログインIDまたはパスワードに値がない時
		if (password.equals("") || login_id.equals("")) {
			mav.addObject(login_id);
			mav.addObject(password);
			mav.setViewName("Login");
			return mav;
		} else {
			// ログインIDとパスワードの両方をセレクト(パスワード重複の可能性があるため)
			String sql = "SELECT login_id FROM member WHERE password =  ? AND login_id = ?";
			SqlRowSet rs = template.queryForRowSet(sql, password, login_id);
			String str = "";
			while (rs.next()) {
				str = rs.getString("login_id");
			}
			
			// ログインIDとパスワードが同じなら次の画面へ遷移 
			if (str.equals(login_id)) {
				mav.setViewName("selection");
				mav.addObject("login_id", login_id);
				return mav;
			} else {
			// ここの処理でJSでアラートを出したい。
				mav.addObject("login_id", login_id);
				mav.addObject("password", password);
				return mav;
			}
		}

		
	}

}
