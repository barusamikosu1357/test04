package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class dailyReportRegisterController {

	@Autowired
	private JdbcTemplate template;

	@RequestMapping(value = "/dailyReportRegister",method = RequestMethod.POST)
	public ModelAndView index(Model model, ModelAndView mav, @ModelAttribute(name = "login_id") String loginId) {
		
		//確認者か報告者かの確認
		String rankSql = "SELECT rank_id"
				+ " FROM member "
				+ "WHERE login_id = ?";
		SqlRowSet rank = template.queryForRowSet(rankSql, loginId);
		String rankId = "";
		while(rank.next()) {
			rankId = rank.getString("rank_id");
		}
		mav.addObject("rank",rankId);
		if(loginId.length() == 0) {
			mav.setViewName("error");
			return mav;
		}else {
		Map<String, String> names = new LinkedHashMap<String, String>();

		//login_idから名前を取得		

		String logInSql = "SELECT name "
				+ "FROM member "
				+ "WHERE login_id = ?";
		SqlRowSet srs = template.queryForRowSet(logInSql, loginId);
		String name = "";
		while (srs.next()) {
			name = srs.getString("name");
		}
		mav.setViewName("dailyReportRegister");
		mav.addObject("names", name);

		return mav;
		}
	}

	//登録ボタンの動作
	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public ModelAndView touroku(ModelAndView mav, @ModelAttribute(name = "title") String title,
			@ModelAttribute(name = "dailyReportText") String dailyReportText,
			@ModelAttribute(name = "login_id") String loginId, @ModelAttribute(name = "day") String day
			,@ModelAttribute(name = "names") String name) {
		
		Date date = new Date();
		if (dailyReportText.length() == 0) {
			dailyReportText = null;
		} else if (title.length() == 0) {
			title = null;
		}

		if (title == null || dailyReportText == null) {
			mav.setViewName("error");
			return mav;

		} else {
			//担当者IDの取得
			List<String> id = new ArrayList<String>();

			//SQL定義
			String sql = "SELECT login_id as id "
					+ "FROM member";

			//SQL実行
			SqlRowSet ids = template.queryForRowSet(sql);
			while (ids.next()) {
				String sId = ids.getString("id");
				id.add(sId);
			}

			//SQL定義
			sql = "BEGIN; "
					+ "INSERT INTO dailyreport(title, content, day, updateday, login_id)"
					+ "   VALUES (?, ?, ?, ?, ?);"
					+ " COMMIT;";
			//SQL実行
			template.update(sql, title, dailyReportText, day, date, loginId);

		}

		mav.addObject("text", title);
		mav.addObject("text", dailyReportText);
		mav.addObject("text", loginId);
		mav.addObject("text", day);
		mav.addObject("text", name);
		mav.setViewName("check");
		return mav;
	}
}