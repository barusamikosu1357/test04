package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Form.DailyReportListForm;

@Controller
public class ListController {
	
	@Autowired
	private JdbcTemplate template;
	
	@RequestMapping("/list")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("list");
		List<DailyReportListForm> list = new ArrayList<>();
		DailyReportListForm drlf = new DailyReportListForm();
		drlf.setName("あいうえお");
		drlf.setTitle("2023/06/21");
		drlf.setStatus("未読");
		drlf.setCreateDate("2023/06/21");
		drlf.setUpdateDate("2023/06/21");
		list.add(drlf);
		list.add(drlf);
		mav.addObject("dailyReportList", list);
		return mav;
	}
	
	/* 以下の内容は、リスト一覧画面作成時に使用する可能性があるため、19行目～のselect文を記述してあります。 */
	public ModelAndView view(ModelAndView mav, @RequestParam(name = "login_id", required = false) String login_id) {
		String sql1 = "SELECT member.name, "
				+ "dailyreport.title, "
				+ "dailyreport.approval_flg, "
				+ "dailyreport.day, "
				+ "dailyreport.updateday "
				+ "FROM member,dailyreport"
				+ "WHERE member.login_id = ? ";
		
		SqlRowSet rs1 = template.queryForRowSet(sql1, login_id);
		String str1 = "";
		while (rs1.next()) {
			str1 = rs1.getString("login_id");
		}
		return mav;
	}

}
