package com.example.demo.Form;

public class DailyReportListForm {

	/* 名前 */
	private String name;

	/* 題名 */
	private String title;

	/* ステータス */
	private String status;

	/* 日付 */
	private String createDate;

	/* 更新日 */
	private String updateDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	};

}
